package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        System.out.println("Lower bound? ");
        int input1 = Integer.parseInt(Keyboard.readInput());
        System.out.println("upper bound? ");
        int input2 = Integer.parseInt(Keyboard.readInput());
        int lower = Math.min(input1, input2);
        int upper = Math.max(input1, input2);
        int first_random_number = (int) (Math.random() * (upper - lower+1) + lower);
        int second_random_number = (int) (Math.random() * (upper - lower+1) + lower);
        int thrid_random_number = (int) (Math.random() * (upper - lower+1) + lower);
        int smallest = Math.min(Math.min(first_random_number, second_random_number), Math.min(second_random_number, thrid_random_number));
        System.out.println("Lower bound? " + lower);
        System.out.println("Upper bound? " + upper);
        System.out.println("3 randomly generated numbers: " + first_random_number +" " + second_random_number +" and " + thrid_random_number);
        System.out.println("Smallest number is " + smallest);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}

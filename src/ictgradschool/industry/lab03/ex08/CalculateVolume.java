package ictgradschool.industry.lab03.ex08;

/**
 * Created by ljam763 on 13/03/2017.
 */
import ictgradschool.Keyboard;

public class CalculateVolume {

    private double radius;

    private CalculateVolume(double radius) {

        this.radius = radius;

    }

    public void start() {

        System.out.println("\"Volume of a Sphere\"");

        System.out.print("Enter the radius: ");

        radius = Double.parseDouble(Keyboard.readInput());

        double volume = (4.0 / 3) * Math.PI * Math.pow(radius, 3);

        System.out.println("Volume: " + volume);

    }


    public static void main(String[] args) {

        CalculateVolume sphere1 = new CalculateVolume(6.5);

        sphere1.start();

    }
}
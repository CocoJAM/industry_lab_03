package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {
    print_number_changed();
        // TODO Use other methods you create to implement this program's functionality.
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    private double input() {
        System.out.print("Please enter an amount: ");
        return Double.parseDouble(Keyboard.readInput());
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    private  int decimal_place(){
        System.out.print("Please enter the number of decimal places: ");
        return Integer.parseInt(Keyboard.readInput());
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    private double truncating() {
        double number = input();
        int decimal = decimal_place();
        number = (int) (number * Math.pow(10, decimal));
        number = number / Math.pow(10, decimal);
        return number;
    }

    // TODO Write a method which prints the truncated amount
    private void print_number_changed () {
        System.out.println(truncating());
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
